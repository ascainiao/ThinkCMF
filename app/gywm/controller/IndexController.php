<?php
namespace app\gywm\controller;

use cmf\controller\HomeBaseController;
use app\portal\model\PortalPostModel;
use app\portal\model\PortalCategoryModel;

class IndexController extends HomeBaseController
{
    public function index()
    {
        return $this->fetch(':about');
        //return 'hello!';
    }
}