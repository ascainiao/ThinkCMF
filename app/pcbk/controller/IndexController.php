<?php
namespace app\pcbk\controller;

use cmf\controller\HomeBaseController;

class IndexController extends HomeBaseController
{
    public function index()
    {
        //$id = $this->request->param('id', 0, 'intval');
        
        //$this->assign('seo_id',$id);
        return $this->fetch(':articlelist');
        //return 'hello!';
    }
}