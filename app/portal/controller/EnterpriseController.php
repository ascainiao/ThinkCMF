<?php
namespace app\portal\controller;

use cmf\controller\HomeBaseController;

class EnterpriseController extends HomeBaseController
{
    public function index()
    {
        $id = $this->request->param('id', 0, 'intval');
        
        $this->assign('seo_id',$id);
        return $this->fetch(':enterprise');
        //return 'hello!';
    }
}