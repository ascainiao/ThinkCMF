<?php
namespace app\portal\controller;

use cmf\controller\HomeBaseController;
use app\portal\model\PortalPostModel;
use app\portal\model\PortalCategoryModel;

class VideodetailsController extends HomeBaseController
{
    public function index()
    {
        $categoryid=$this->request->param('categoryid', 0, 'intval');
        $portalCategoryModel = new PortalCategoryModel();
        $category = $portalCategoryModel->where('id', $categoryid)->find();
        //print($category);
        $this->assign('category', $category);

        $id = $this->request->param('id', 0, 'intval');
        $portalPostModel = new PortalPostModel();
        $postcategory = $portalPostModel->where('id', $id)->where('post_status', 1)->find();
        //print($postcategory);
        $this->assign('postcategory',$postcategory);
        return $this->fetch(':videodetails');
        //return 'hello!';
    }
}