<?php
namespace app\portal\controller;

use cmf\controller\HomeBaseController;
use app\portal\model\PortalPostModel;
use app\portal\model\PortalCategoryModel;

class ArticledetailsController extends HomeBaseController
{
    public function index()
    {
        //文章分类
        $categoryid=$this->request->param('categoryid', 0, 'intval');
        $portalCategoryModel = new PortalCategoryModel();
        $category = $portalCategoryModel->where('id', $categoryid)->find();
        //print($category);
        $this->assign('category', $category);
        //文章
        $id = $this->request->param('id', 0, 'intval');
        $portalPostModel = new PortalPostModel();
        $postcategory = $portalPostModel->where('id', $id)->where('post_status', 1)->find();
        //print($postcategory);
        $this->assign('postcategory',$postcategory);

        $prev_post=$portalPostModel->where('id', $id-1)->where('post_status', 1)->find();
        $next_post=$portalPostModel->where('id', $id+1)->where('post_status', 1)->find();
        $this->assign('prev_post',$prev_post);
        $this->assign('next_post',$next_post);
        //增加阅读量
        $portalPostModel->where('id', $id)->inc('post_hits')->update();
        return $this->fetch(':articledetails');
        //return 'hello!';
    }
}