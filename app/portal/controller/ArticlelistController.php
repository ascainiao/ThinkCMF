<?php
namespace app\portal\controller;

use cmf\controller\HomeBaseController;

class ArticlelistController extends HomeBaseController
{
    public function index()
    {
        return $this->fetch(':articlelist');
        //return 'hello!';
    }
}