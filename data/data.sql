USE `thinkcmf`;

/*Data for the table `cmf_asset` */

insert  into `cmf_asset`(`id`,`user_id`,`file_size`,`create_time`,`status`,`download_times`,`file_key`,`filename`,`file_path`,`file_md5`,`file_sha1`,`suffix`,`more`) values 
(2,1,35801,1644999935,1,0,'c5dabfb42a09b5d368d276390ce41bd3cfe7b8ba71235abdd1d1dbb0b6f6be12','logo.jpg','default/20220216/9acbc9b81ae1dc5412bb23ddef8f900d.jpg','c5dabfb42a09b5d368d276390ce41bd3','f262a0fe388cce5cc08f99f1f6ea31c9a6936007','jpg',NULL),
(3,1,698229,1645008460,1,0,'a5a16b9ae0e20a61ba8ce2d9a6a841aa2e58e8fcc4ac60434c3f7de926ed3079','a5cc3005b733149ef.jpeg','default/20220216/82770c47b4d9917567b498a8d4a0d130.jpeg','a5a16b9ae0e20a61ba8ce2d9a6a841aa','15a09aa86cf9fd4746e90f1fd65eb0704e19b42d','jpeg',NULL),
(4,1,128471,1645153351,1,0,'9232c667791517b0ce6221274c76f4ddf632ba2a496d29f81127c77c9f65b189','微信截图_20220218110201.png','portal/20220218/ebc23713160a2391a54d8b9284362ece.png','9232c667791517b0ce6221274c76f4dd','26ec61404607e22881fa8adcf9ebf19ee77a350e','png',NULL);

/*Data for the table `cmf_nav_menu` */

insert  into `cmf_nav_menu`(`id`,`nav_id`,`parent_id`,`status`,`list_order`,`name`,`target`,`href`,`icon`,`path`) values 
(2,1,0,1,10000,'企业宣传片','','#','',''),
(3,1,0,1,10000,'产品广告片','','#','',''),
(4,1,0,1,10000,'三维动画制作','','#','',''),
(5,1,0,1,10000,'活动直播拍摄','','#','',''),
(6,1,0,1,10000,'短视频','','#','',''),
(7,1,0,1,10000,'案例欣赏','','#','',''),
(8,1,0,1,10000,'品策百科','','#','',''),
(9,1,0,1,10000,'关于我们','','#','','');

/*Data for the table `cmf_option` */

insert  into `cmf_option`(`id`,`autoload`,`option_name`,`option_value`) values 
(1,1,'site_info','{\"site_name\":\"ThinkCMF内容管理框架\",\"site_seo_title\":\"广州产品宣传片制作_广州企业宣传片拍摄_企业宣传片制作-广州品策文化公司\",\"site_seo_keywords\":\"宣传片拍摄,宣传片制作,企业宣传片拍摄,企业宣传片制作,广州宣传片拍摄,广州宣传片制作,视频制作公司,产品宣传片拍摄,产品宣传片制作,广州视频拍摄\",\"site_seo_description\":\"企业宣传片制作哪家靠谱，推荐广州品策文化，专注宣传片制作领域十多年，拥有专业影视人才，从策划，拍摄，制作一站式完成；力争打造广州地区性价比最高的宣传片制作公司；咨询热线：13430231012\",\"site_icp\":\"\",\"site_gwa\":\"\",\"site_admin_email\":\"\",\"site_analytics\":\"\"}'),
(2,1,'cmf_settings','{\"open_registration\":\"0\",\"banned_usernames\":\"\"}'),
(3,1,'cdn_settings','{\"cdn_static_root\":\"\"}'),
(4,1,'admin_settings','{\"admin_password\":\"\",\"admin_theme\":\"admin_simpleboot3\",\"admin_style\":\"simpleadmin\"}');

/*Data for the table `cmf_portal_category` */

insert  into `cmf_portal_category`(`id`,`parent_id`,`post_count`,`status`,`delete_time`,`list_order`,`name`,`description`,`path`,`seo_title`,`seo_keywords`,`seo_description`,`list_tpl`,`one_tpl`,`more`) values 
(1,0,0,1,0,10,'核心业务','','','','','','','',NULL),
(2,0,0,1,0,20,'我们的优势','','','','','','','',NULL);

/*Data for the table `cmf_portal_category_post` */

insert  into `cmf_portal_category_post`(`id`,`post_id`,`category_id`,`list_order`,`status`) values 
(1,1,1,10000,1),
(2,2,1,10000,1),
(3,3,1,10000,1),
(4,4,1,10000,1),
(5,5,2,10000,1),
(6,6,2,10000,1),
(7,7,2,10000,1);

/*Data for the table `cmf_portal_post` */

insert  into `cmf_portal_post`(`id`,`parent_id`,`post_type`,`post_format`,`user_id`,`post_status`,`comment_status`,`is_top`,`recommended`,`post_hits`,`post_favorites`,`post_like`,`comment_count`,`create_time`,`update_time`,`published_time`,`delete_time`,`post_title`,`post_keywords`,`post_excerpt`,`post_source`,`thumbnail`,`post_content`,`post_content_filtered`,`more`) values 
(1,0,1,1,1,1,1,0,0,0,0,0,0,1645153354,1645161238,1645152720,0,'企业宣传片拍摄','','','','portal/20220218/ebc23713160a2391a54d8b9284362ece.png','&lt;p&gt;&lt;span style=&quot;color: rgb(51, 51, 51); font-family: 宋体; white-space: pre-wrap; background-color: rgb(255, 255, 255); font-size: 18px;&quot;&gt;80字以内介绍文案80字以内介绍文案80字以内介绍文案80字以内介绍文案80字以内介绍文案80字以内介绍文案80字以内介绍文案&lt;/span&gt;&lt;/p&gt;',NULL,'{\"audio\":\"\",\"video\":\"\",\"thumbnail\":\"portal\\/20220218\\/ebc23713160a2391a54d8b9284362ece.png\",\"template\":\"\"}'),
(2,0,1,1,1,1,1,0,0,0,0,0,0,1645153458,1645161259,1645153380,0,'产品广告片制作','','','','portal/20220218/ebc23713160a2391a54d8b9284362ece.png','&lt;p&gt;&lt;span style=&quot;color: rgb(51, 51, 51); font-family: 宋体; white-space: pre-wrap; background-color: rgb(255, 255, 255); font-size: 18px;&quot;&gt;80字以内介绍文案80字以内介绍文案80字以内介绍文案80字以内介绍文案80字以内介绍文案80字以内介绍文案80字以内介绍文案&lt;/span&gt;&lt;/p&gt;',NULL,'{\"audio\":\"\",\"video\":\"\",\"thumbnail\":\"portal\\/20220218\\/ebc23713160a2391a54d8b9284362ece.png\",\"template\":\"\"}'),
(3,0,1,1,1,1,1,0,0,0,0,0,0,1645153565,1645161272,1645153500,0,'三维动画制作','','','','portal/20220218/ebc23713160a2391a54d8b9284362ece.png','&lt;p&gt;&lt;span style=&quot;color: rgb(51, 51, 51); font-family: 宋体; white-space: pre-wrap; background-color: rgb(255, 255, 255); font-size: 18px;&quot;&gt;80字以内介绍文案80字以内介绍文案80字以内介绍文案80字以内介绍文案80字以内介绍文案80字以内介绍文案80字以内介绍文案&lt;/span&gt;&lt;/p&gt;',NULL,'{\"audio\":\"\",\"video\":\"\",\"thumbnail\":\"portal\\/20220218\\/ebc23713160a2391a54d8b9284362ece.png\",\"template\":\"\"}'),
(4,0,1,1,1,1,1,0,0,0,0,0,0,1645158356,1645161290,1645158300,0,'活动直播拍摄','','','','portal/20220218/ebc23713160a2391a54d8b9284362ece.png','&lt;p&gt;&lt;span style=&quot;color: rgb(51, 51, 51); font-family: 宋体; white-space: pre-wrap; background-color: rgb(255, 255, 255); font-size: 18px;&quot;&gt;80字以内介绍文案80字以内介绍文案80字以内介绍文案80字以内介绍文案80字以内介绍文案80字以内介绍文案80字以内介绍文案&lt;/span&gt;&lt;/p&gt;',NULL,'{\"audio\":\"\",\"video\":\"\",\"thumbnail\":\"portal\\/20220218\\/ebc23713160a2391a54d8b9284362ece.png\",\"template\":\"\"}'),
(5,0,1,1,1,1,1,0,0,0,0,0,0,1645180202,1645180209,1645179660,0,'10年','','','','','&lt;p&gt;&lt;span style=&quot;color: rgb(51, 51, 51); font-family: 宋体; white-space: pre-wrap; background-color: rgb(255, 255, 255); font-size: 18px;&quot;&gt;十年经验文案内容十年经验文案内容十年经验文案内容十年经验文案&lt;/span&gt;&lt;/p&gt;',NULL,'{\"audio\":\"\",\"video\":\"\",\"thumbnail\":\"\",\"template\":\"\"}'),
(6,0,1,1,1,1,1,0,0,0,0,0,0,1645180268,1645180277,1645180200,0,'1000+','','','','','&lt;p&gt;1000多家服务企业文案内容1000多家服务企业文案内容&lt;span style=&quot;font-size: 18px;&quot;&gt;&lt;/span&gt;&lt;/p&gt;',NULL,'{\"audio\":\"\",\"video\":\"\",\"thumbnail\":\"\",\"template\":\"\"}'),
(7,0,1,1,1,1,1,0,0,0,0,0,0,1645180314,1645180317,1645180260,0,'一站式','','','','','&lt;p&gt;视频拍摄一站式服务文案内容（其他优势也可以）&lt;/p&gt;',NULL,'{\"audio\":\"\",\"video\":\"\",\"thumbnail\":\"\",\"template\":\"\"}');

/*Data for the table `cmf_slide` */

insert  into `cmf_slide`(`id`,`status`,`delete_time`,`name`,`remark`) values 
(1,1,0,'首页顶部幻灯片','在首页顶部显示的幻灯片');

/*Data for the table `cmf_theme` */

insert  into `cmf_theme`(`id`,`create_time`,`update_time`,`status`,`is_compiled`,`theme`,`name`,`version`,`demo_url`,`thumbnail`,`author`,`author_url`,`lang`,`keywords`,`description`) values 
(2,0,0,0,0,'pincetv','pincetv','1.0.0','http://demo.thinkcmf.com','','qiao','http://www.thinkcmf.com','zh-cn','ThinkCMF默认模板','ThinkCMF默认模板'),
(3,0,0,0,0,'simpleboot3','simpleboot3','1.0.2','http://demo.thinkcmf.com','','ThinkCMF','http://www.thinkcmf.com','zh-cn','ThinkCMF模板','ThinkCMF默认模板');
